package br.com.strategy.pagamento;

/**
 * @author Gabriel Hideki
 * 
 * Interface comum de m�todos de pagamento.
 */
public interface PayStrategy {

	boolean pay(int paymentAmount);

	void collectPaymentDetails();

}
