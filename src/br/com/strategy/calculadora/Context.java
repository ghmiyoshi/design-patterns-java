package br.com.strategy.calculadora;

import br.com.strategy.calculadora.operacao.Strategy;

/**
 * @author Gabriel Hideki
 * 
 * O contexto define a interface de interesse para clientes.
 */
public class Context {

	/* O contexto mant�m uma refer�ncia para um dos objetos estrat�gia. O contexto
	 * n�o sabe a classe concreta de uma estrat�gia. Ele deve trabalhar com todas as
	 * estrat�gias atrav�s da interface estrat�gia. */
	private Strategy strategy;

	public Context(Strategy strategy) {
		this.strategy = strategy;
	}

	/* Geralmente o contexto aceita uma estrat�gia atrav�s do construtor, e tamb�m
	 * fornece um setter para que a estrat�gia possa ser trocado durante o tempo de
	 * execu��o. */
	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

	/* O contexto delega algum trabalho para o objeto estrat�gia ao inv�s de
	 * implementar m�ltiplas vers�es do algoritmo por conta pr�pria. */
	public int executeStrategy(int a, int b) {
		return this.strategy.execute(a, b);
	}

}
