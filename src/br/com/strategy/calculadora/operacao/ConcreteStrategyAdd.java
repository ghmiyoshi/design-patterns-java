package br.com.strategy.calculadora.operacao;

/**
 * @author Gabriel Hideki
 * 
 * Estrat�gias concretas implementam o algoritmo enquanto seguem a
 * interface estrat�gia base. A interface faz delas intercomunic�veis no
 * contexto.
 */
public class ConcreteStrategyAdd implements Strategy {

	@Override
	public int execute(int a, int b) {
		return a + b;
	}

}
