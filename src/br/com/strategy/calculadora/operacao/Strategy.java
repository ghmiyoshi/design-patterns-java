package br.com.strategy.calculadora.operacao;

/**
 * @author Gabriel Hideki
 * 
 * A interface estrat�gia declara opera��es comuns a todas as vers�es
 * suportadas de algum algoritmo. O contexto usa essa interface para
 * chamar o algoritmo definido pelas estrat�gias concretas.
 */
public interface Strategy {

	int execute(int a, int b);

}
