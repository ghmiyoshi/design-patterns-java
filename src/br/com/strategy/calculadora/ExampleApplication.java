package br.com.strategy.calculadora;

import br.com.strategy.calculadora.operacao.ConcreteStrategyAdd;
import br.com.strategy.calculadora.operacao.ConcreteStrategyMultiply;

/**
 * @author Gabriel Hideki
 *
 * O c�digo cliente escolhe uma estrat�gia concreta e passa ela para o
 * contexto. O cliente deve estar ciente das diferen�as entre as
 * estrat�gia para que fa�a a escolha certa.
 */
public class ExampleApplication {
	
	public static void main(String[] args) {
		Context context = new Context(new ConcreteStrategyAdd());
		System.out.println("Result: " + context.executeStrategy(10, 5));

		context.setStrategy(new ConcreteStrategyMultiply());
		System.out.println("Result: " + context.executeStrategy(10, 5));
	}

}
