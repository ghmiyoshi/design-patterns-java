package br.com.strategy.loja.imposto;

import java.math.BigDecimal;

import br.com.strategy.loja.Orcamento;

public class ICMS implements Imposto {

	@Override
	public BigDecimal calcular(Orcamento orcamento) {
		return orcamento.getValor().multiply(BigDecimal.valueOf(0.1));
	}

}
