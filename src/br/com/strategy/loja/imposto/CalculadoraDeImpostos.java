package br.com.strategy.loja.imposto;

import java.math.BigDecimal;

import br.com.strategy.loja.Orcamento;

public class CalculadoraDeImpostos {

	public BigDecimal calcular(Orcamento orcamento, Imposto imposto) {
		return imposto.calcular(orcamento);
	}

}
