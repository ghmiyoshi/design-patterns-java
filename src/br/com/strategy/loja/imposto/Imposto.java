package br.com.strategy.loja.imposto;

import java.math.BigDecimal;

import br.com.strategy.loja.Orcamento;

public interface Imposto {

	BigDecimal calcular(Orcamento orcamento);

}
