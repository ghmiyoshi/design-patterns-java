package br.com.strategy.loja.imposto;

import java.math.BigDecimal;

import br.com.strategy.loja.Orcamento;

public class ISS implements Imposto {

	@Override
	public BigDecimal calcular(Orcamento orcamento) {
		return orcamento.getValor().multiply(new BigDecimal("0.06"));
	}

}
