package br.com.strategy.loja;

import java.math.BigDecimal;

import br.com.strategy.loja.imposto.CalculadoraDeImpostos;
import br.com.strategy.loja.imposto.ICMS;

public class Aplicacao {
	
	public static void main(String[] args) {
		Orcamento orcamento = new Orcamento(new BigDecimal("100"), 1);
		CalculadoraDeImpostos calculadoraDeImpostos = new CalculadoraDeImpostos();
		
		System.out.println(calculadoraDeImpostos.calcular(orcamento, new ICMS()));
	}

}
