package br.com.chainofresponsibility.desconto;

import java.math.BigDecimal;

import br.com.strategy.loja.Orcamento;

public class Aplicacao {

	public static void main(String[] args) {
		CalculadoraDeDescontos calculadoraDeDescontos = new CalculadoraDeDescontos();
		Orcamento orcamento = new Orcamento(new BigDecimal("2000"), 4);

		System.out.println(calculadoraDeDescontos.calcular(orcamento));
	}

}
