package br.com.chainofresponsibility.desconto;

import java.math.BigDecimal;

import br.com.strategy.loja.Orcamento;

public class SemDesconto extends Desconto {

	@Override
	public BigDecimal efetuarCalculo(Orcamento orcamento) {
		return BigDecimal.ZERO;
	}

	@Override
	public boolean deveAplicar(Orcamento orcamento) {
		return true;
	}

}
