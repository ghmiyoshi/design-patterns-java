package br.com.chainofresponsibility.desconto;

import java.math.BigDecimal;

import br.com.strategy.loja.Orcamento;

public abstract class Desconto {

	protected Desconto proximoDesconto;

	public Desconto(Desconto desconto) {
		this.proximoDesconto = desconto;
	}

	public Desconto() {
	}

	/* Design pattern Template method */
	public BigDecimal calcular(Orcamento orcamento) {
		if (deveAplicar(orcamento)) {
			return efetuarCalculo(orcamento);
		}
		return proximoDesconto.calcular(orcamento);
	}

	protected abstract BigDecimal efetuarCalculo(Orcamento orcamento);

	protected abstract boolean deveAplicar(Orcamento orcamento);

}
