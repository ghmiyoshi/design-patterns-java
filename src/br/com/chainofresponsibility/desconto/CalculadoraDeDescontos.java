package br.com.chainofresponsibility.desconto;

import java.math.BigDecimal;

import br.com.strategy.loja.Orcamento;

public class CalculadoraDeDescontos {

	public BigDecimal calcular(Orcamento orcamento) {
		Desconto cadeiaDeDescontos = new DescontoParaOrcamentoComMaisDeCincoItens(
						    new DescontoParaOrcamentoComValorMaiorQueQuinhentos(
						    new SemDesconto()));
		return cadeiaDeDescontos.calcular(orcamento);
	}

}
